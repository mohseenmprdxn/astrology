
$(document).ready(function () {
//  accordion functionality for aside section
    $('.accordion').each(function () {
        var $accordian = $(this);
        $accordian.find('.accordion-head').on('click', function () {
            $(this).parent().find(".accordion-head").removeClass('open close');
            $(this).removeClass('open').addClass('close');
            $accordian.find('.accordion-body').slideUp();
            if (!$(this).next().is(':visible')) {
                $(this).removeClass('close').addClass('open');
                $(this).next().slideDown();
            }
        });
    });
    // slider functionality for personal horoscope section
    $('.personal-slider').slick({
        Arrows: true,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
      });

    //   back-to-top functionality 
    
    
    var backTop = $('.to-top a');
    $(window).scroll(function () {

        if ($(this).scrollTop() > 450 ) {
            $(backTop).fadeIn();
        } else {
            $(backTop).fadeOut();
        }
    });

    $(backTop).click(function () {
        $("html, body").animate({scrollTop: 0},1000);
     });
              
});